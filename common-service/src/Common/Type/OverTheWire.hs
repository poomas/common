{-# LANGUAGE FunctionalDependencies, TemplateHaskell #-}
module Common.Type.OverTheWire where

import Imports


-- | Convert records from DB shape into OTW shape
class FromWire dbRec otwRec where otwToDb :: otwRec -> Maybe dbRec
class ToWire   dbRec otwRec where dbToOtw :: dbRec  ->       otwRec

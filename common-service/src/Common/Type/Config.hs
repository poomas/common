{-# OPTIONS_GHC -fno-warn-orphans #-}
module Common.Type.Config where

import           Imports

import qualified Data.Char                     as C
import           Data.Swagger                  (ToSchema (..), description,
                                                fromAesonOptions,
                                                genericDeclareNamedSchema, schema)
import           Data.Yaml                     (decodeEither')
import           Data.Yaml.Config              (loadYamlSettings, useCustomEnv, useEnv)
import           Network.Info                  as NI (NetworkInterface (..),
                                                      getNetworkInterfaces)
import           Network.Socket.Free           (getFreePort)
import qualified RIO.HashMap                   as HM


import           Poomas.API                    (NeedData (..), NeedDataFun,
                                                PoomasConfig (..), PortSetter (..),
                                                getPort)
import           Scribe.Class                  (AnyScribe (..))



data CommonConfig = CommonConfig
  { cCfgAppHost           :: Text                  -- ^ http/s address
  , cCfgAppPort           :: Word                  -- ^ port on which the app is run
  , cCfgAppName           :: Text                  -- ^ identification of this app
  , cCfgPoomas            :: PoomasConfig          -- ^ list of initial hub hosts to query
  , cCfgIsDev             :: Bool                  -- ^ is app in development mode
  , cCfgRequestTimeout    :: Maybe Int             -- ^ seconds to timeout `Request`
  , cCfgEkg               :: Maybe EkgConfig       -- ^ host and port for EKG server
  , cCfgLoggers           :: [AnyScribe]           -- ^ list of backends for log output
  , cCfgLogRequest        :: Bool                  -- ^ log each HTTP request?
  , cCfgLogToPoomas       :: Bool                  -- ^ log events to poomas?
  , cCfgFullSiteBasicAuth :: Maybe BasicAuthConfig -- ^ Basic HTTP Auth info
  , cCfgStorageDir        :: FilePath              -- ^ the main file storage
  , cCfgAlwaysAddHeaders  :: [(String,String)]     -- ^ static headers for each `Response'
  } deriving (Show, Eq, Generic)
instance ToJSON   CommonConfig where toJSON = genericToJSON $ jsonOpts 3

data HostSetter = HostAlways  String
                | HostIfIFace [(String,String)]
                | HostNever
data AppConfigData = AppConfigData
  { hostNameSetter   :: HostSetter
  , hostPortSetter   :: PortSetter
  , ekgPortSetter    :: PortSetter
  , poomasPortSetter :: PortSetter
  , isDev            :: Bool
  }


newtype NeedDataAppConfig a = NeedDataAppConfig {unAppConfigData :: AppConfigData -> a}
instance FromJSON AnyScribe => FromJSON (NeedDataAppConfig CommonConfig) where
  parseJSON = withObject "AppConfig" $ \o -> do
    cCfgAppHost           <- o .:  "appHost"
    cCfgAppPort           <- o .:  "appPort"
    cCfgAppName           <- o .:  "appName"
    cCfgIsDev             <- o .:  "isDev"
    cCfgRequestTimeout    <- o .:? "requestTimeout"    .!= Nothing
    cCfgEkg               <- o .:? "ekg"               .!= Nothing
    cCfgLoggers           <- o .:? "loggers"           .!= []
    cCfgLogRequest        <- o .:? "logRequest"        .!= False
    cCfgLogToPoomas       <- o .:? "logToPoomas"       .!= True
    cCfgFullSiteBasicAuth <- o .:? "fullSiteBasicAuth" .!= Nothing
    cCfgStorageDir        <- o .:  "storageDir"
    cCfgAlwaysAddHeaders  <- o .:? "alwaysAddHeaders"  .!= []
    setPoomData :: NeedDataFun <- unNeedData <$> o .: "poomas"
    let cCfgPoomas = setPoomData (Just $ toS cCfgAppHost) (Just cCfgAppPort) PortNever
        appCac     = CommonConfig { .. }

    pure $ NeedDataAppConfig $ \AppConfigData{..} -> do
      let newHostName = getHost (toS cCfgAppHost) hostNameSetter
          newHostPort = getPort cCfgAppPort hostPortSetter
          newPoomas   = setPoomData (Just newHostName) (Just newHostPort) poomasPortSetter
          newEkg      = case cCfgEkg of
            Nothing              -> cCfgEkg
            Just (EkgConfig p b) -> do
              let b' = if b == "0.0.0.0" then newHostName else b
              Just $ EkgConfig (getPort p ekgPortSetter) b'

      appCac { cCfgAppHost = toS newHostName
             , cCfgAppPort = newHostPort
             , cCfgEkg     = newEkg
             , cCfgPoomas  = newPoomas
             , cCfgIsDev   = isDev
             }
    where
      getHost :: String -> HostSetter -> String
      getHost currVal = \case
        HostNever       -> currVal
        HostAlways h    -> h
        HostIfIFace ifs -> case currVal of
          ('@':ifc) -> fromMaybe (error missingIface) $ lookup ifc ifs
          x         -> x
        where
          missingIface = "ERROR: Network interface '" <> show currVal <> "' not found!"


-- | Record holding embedded data. This data is read during compile time from a file
--   so file does not need be deployed with app
data InternalFileData = InternalFileData
  { ifdDBPassword :: String
  , ifdTLSCert    :: String
  , ifdTLSKey     :: String
  } deriving (Show, Eq, Generic)
instance FromJSON InternalFileData where parseJSON = genericParseJSON $ jsonOpts 3


-- | Host and port for EKG server
data EkgConfig = EkgConfig
  { ekgPort :: Word
  , ekgBind :: String
  } deriving (Show, Eq, Generic)
instance FromJSON EkgConfig where parseJSON = genericParseJSON $ jsonOpts 3
instance ToJSON   EkgConfig where toJSON    = genericToJSON    $ jsonOpts 3
instance ToSchema EkgConfig where
  declareNamedSchema p = genericDeclareNamedSchema (fromAesonOptions $ jsonOpts 3) p
    & mapped .schema.description ?~ "EKG configuration"


-- | Info for setting up the Basic HTTP Auth for the whole site
data BasicAuthConfig = BasicAuthConfig
  { bAuthRealm :: String
  , bAuthCreds :: Text
  , bAuthUser  :: String
  , bAuthPwd   :: String
  } deriving (Show, Eq, Generic)
instance FromJSON BasicAuthConfig where parseJSON = genericParseJSON $ jsonOpts 5
instance ToJSON   BasicAuthConfig where toJSON    = genericToJSON    $ jsonOpts 5
instance ToSchema BasicAuthConfig where
  declareNamedSchema p = genericDeclareNamedSchema (fromAesonOptions $ jsonOpts 5) p
    & mapped .schema.description ?~ "BasicAuth configuration"


-- | YAML file parsed at compile time
compileTimeYaml :: FromJSON c => ByteString -> c
compileTimeYaml = either impureThrow id . decodeEither'


-- | Parses YAML file with configuration. If passed a list with some (ENV name, value),
--   final config is mocked with those values. Names of ENV vars must correspond to
--   ENV vars specified in YAML configuration.
parseCommonConfig :: FromJSON (NeedDataAppConfig a)
                  => Bool -> [FilePath] -> [(Text, Text)] -> IO a
parseCommonConfig isDev fNms mockEnv = do
  let defValues = [object ["isDev" .= isDev]]
      envKind   = bool (useCustomEnv $ HM.fromList mockEnv) useEnv $ null mockEnv
  needDataAC <- unAppConfigData <$> loadYamlSettings fNms defValues envKind
  portHost   <- fromIntegral <$> getFreePort
  portPoomas <- fromIntegral <$> getFreePort
  portEkg    <- fromIntegral <$> getFreePort
  ifs        <- map (\ni -> (NI.name ni, show $ NI.ipv4 ni)) <$> NI.getNetworkInterfaces
  let acp = AppConfigData { hostNameSetter   = HostIfIFace   ifs
                          , hostPortSetter   = PortIfMissing portHost
                          , ekgPortSetter    = PortIfMissing portEkg
                          , poomasPortSetter = PortIfMissing portPoomas
                          , isDev            = isDev
                          }
  pure $ needDataAC acp

-- | Options for automatic FromJSON and ToJSON generation.
jsonOpts :: Int -> Options
jsonOpts n = defaultOptions { fieldLabelModifier = fixField }
 where
  fixField t = case drop n t of
    (x : xs) -> C.toLower x : xs
    _        -> []

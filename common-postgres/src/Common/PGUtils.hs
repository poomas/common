module Common.PGUtils
  ( DbPool (..)
  , HasDbPool (..)
  , HasDbConfig (..)
  , DbConfig (..)
  , mkPgConfig

  , DbAction
  , AppDbAction

  , runDB
  , runDBNoLog
  , runDBEither
  , runDBCfg
  , getBy404
  , get404
  , makePool

  , valToKey
  , txtToKey
  , intToKey
  , keyToInt
  , keyToTxt
  )
where

import           ClassyPrelude
import           Control.Exception.Safe      (MonadThrow, throwM)
import           Control.Monad.Logger        (MonadLoggerIO)
import           Data.Aeson
    ( FromJSON (..)
    , Options (..)
    , ToJSON (..)
    , defaultOptions
    , genericParseJSON
    , genericToJSON
    )
import qualified Data.Char                   as C
import           Data.String.Conv            (toS)
import           Database.Persist            (PersistEntity (..), PersistValue (..))
import           Database.Persist.Postgresql
    ( BaseBackend
    , ConnectionPool
    , Entity (..)
    , PersistStoreRead (..)
    , PersistUniqueRead (..)
    , PostgresConf (..)
    , SqlPersistT
    , createPostgresqlPool
    , get
    , runSqlPool
    )
import           Katip                       (Katip (..), katipNoLogging)
import           Text.Read                   (readMaybe)

import           Servant                     (err404)


-- | Record holding pool for PostgreSQL database
newtype DbPool = DbPool {dbPool :: ConnectionPool }

-- | Class for accessing pool from app state
class HasDbPool m where getDbPool :: m DbPool

-- | Info for connecting to DB with `Persistent`
class HasDbConfig c where getDbConfig :: c -> DbConfig
instance HasDbConfig DbConfig where getDbConfig = id


data DbConfig = DbConfig
  { dbHost     :: Text
  , dbPort     :: Int
  , dbName     :: Text
  , dbUser     :: Text
  , dbPwd      :: Text
  , dbPoolSize :: Int
  } deriving (Show, Eq, Generic)
instance FromJSON DbConfig where parseJSON = genericParseJSON $ jsonOpts 2
instance ToJSON   DbConfig where toJSON    = genericToJSON    $ jsonOpts 2

-- | Make Postgres connection string from supplied data
mkPgConfig :: DbConfig -> PostgresConf
mkPgConfig DbConfig{..} = PostgresConf pgConn 1 1 dbPoolSize
  where
    pgConn = toS $ "dbname="    <> dbName <>
                   " host="     <> dbHost <>
                   " user="     <> dbUser <>
                   " password=" <> dbPwd  <>
                   " port="     <> tshow dbPort

-- | Options for automatic FromJSON and ToJSON generation.
jsonOpts :: Int -> Options
jsonOpts n = defaultOptions { fieldLabelModifier = fixField }
 where
  fixField t = case drop n t of
    (x : xs) -> C.toLower x : xs
    _        -> []


-- | Convenience type for `Persistent` actions passed to below functions
type DbAction a = SqlPersistT IO a


-- | Convenience type for shortening function signature
type AppDbAction b r m = ( MonadIO m
                         , MonadThrow m
                         , PersistStoreRead b
                         , PersistEntity r
                         , PersistEntityBackend r ~ BaseBackend b
                         )

-- | Run `DbAction` within any `Monad` supporting `HasDbPool`
runDB :: (HasDbPool m, MonadIO m) => DbAction a -> m a
runDB a = (liftIO . runSqlPool a) . dbPool =<< getDbPool

-- | Run `DbAction` within any `Monad` supporting `HasDbPool`, but without logging, if any
runDBNoLog :: (Katip m, HasDbPool m) => DbAction a -> m a
runDBNoLog a = katipNoLogging $ (liftIO . runSqlPool a) . dbPool =<< getDbPool


-- | Run `DbAction` within any `Monad` supporting `HasDbPool`, but catch any errors
--   and return an Either
runDBEither :: (HasDbPool m, Exception e, MonadUnliftIO m) => DbAction b -> m (Either e b)
runDBEither = try . runDB

-- | Run `DbAction` with supplied `DbPool`
runDBCfg :: MonadUnliftIO m => DbPool -> SqlPersistT m a -> m a
runDBCfg DbPool{..} a = runSqlPool a dbPool

-- | Fetch a row from DB based on its `Unique` value. If the row is not found,
--   throw `Servant`s 404 exception
getBy404 :: (AppDbAction b r m, PersistUniqueRead b) => Unique r -> ReaderT b m (Entity r)
getBy404 u = maybe (throwM err404) pure =<< getBy u

-- | Fetch a row from DB based on its ID. If the row is not found, throw `Servant`s 404
--   exception
get404 :: AppDbAction b r m => Key r -> ReaderT b m r
get404 k = maybe (throwM err404) pure =<< get k


-- | Create PostgreSql connection pool
makePool :: (MonadUnliftIO m, MonadLoggerIO m) => PostgresConf -> m ConnectionPool
makePool PostgresConf{..} = createPostgresqlPool pgConnStr pgPoolSize


-- | Convert 'Key' to 'Integer'
keyToInt :: PersistEntity record => Key record -> Maybe Int64
keyToInt k = case keyToValues k of
  [PersistInt64 i] -> Just i
  _                -> Nothing

-- | Convert 'Integer' to 'Key'
intToKey :: PersistEntity record => Int64 -> Maybe (Key record)
intToKey i = case keyFromValues [PersistInt64 i] of
  Right k -> Just k
  _       -> Nothing

-- | Convert 'Key' to 'Text'
keyToTxt :: PersistEntity record => Key record -> Maybe Text
keyToTxt k = case keyToValues k of
  [PersistText i] -> Just i
  _               -> Nothing

-- | Convert 'Text' to 'Key'
txtToKey :: PersistEntity record => Text -> Maybe (Key record)
txtToKey i = case keyFromValues [PersistText i] of
  Right k -> Just k
  _       -> Nothing

-- | Convert some value to 'Key'
valToKey :: (Read a, Show v) => v -> Maybe a
valToKey = readMaybe . show

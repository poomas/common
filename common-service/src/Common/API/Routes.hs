module Common.API.Routes where

import Imports

import Servant.API.Generic    (ToServant)
import Servant.Server.Generic (AsServerT, genericServerT)


import Common.API.Admin       (CommonAdminRoutes (..))
import Common.Functions       (getAllSeverities, setScribeSeverity, shutdownMaster)
import Common.Type.App        (HasCommonApp (..), getPhcEnv)
import Poomas.API             (KnownHubs)
import Poomas.Server          (getSwarmState)
import Scribe.Katip           (logErrorKS, logInfoK, logWarnK)

------------------------------------------------------------------------------------------
-- Routes
------------------------------------------------------------------------------------------
commonAdminApi :: (KatipContext m, HasCommonApp m, MonadThrow m)
               => ToServant CommonAdminRoutes (AsServerT m)
commonAdminApi = genericServerT CommonAdminRoutes
  { carDebugSetR   = postDebugH
  , carDebugInfoR  = getDebugH
  , carShutdownR   = shutdownServerH
  , carAliveR      = aliveH
  , carSwarmStateR = swarmStateH
  }


------------------------------------------------------------------------------------------
-- Handlers
------------------------------------------------------------------------------------------

-- | Handler for shutting down the server
shutdownServerH :: (KatipContext m, HasCommonApp m) => m Text
shutdownServerH = do
  logWarnK "Stopping server"
  shutdownMaster
  pure "If you received this, server didn't stop"

-- | Handler for changing Severity to display more or less info in logs
postDebugH :: (KatipContext m, HasCommonApp m) => Text -> Severity -> m Text
postDebugH scribeName severity = do
  setScribeSeverity scribeName severity >>= \case
    True -> logInfoK okMsg >> pure okMsg
    False -> do
      logErrorKS "postDebug" $ "Bad scribe name: " <> scribeName
      pure $ "Scribe '" <> scribeName <> "' not found"

  where
    okMsg :: StringConv Text s => s
    okMsg = toS  $ "Changed debug permit for : " <> scribeName


-- | Handler for getting current Severity
getDebugH :: (KatipContext m, HasCommonApp m) => m [(Text,Severity)]
getDebugH = getAllSeverities

-- | Handler for returning the swarm state obtained from local hub
swarmStateH :: (KatipContext m, HasCommonApp m, MonadThrow m) => m KnownHubs
swarmStateH = do
  phcEnv <- getPhcEnv
  hh     <- liftIO $ getSwarmState phcEnv
  case hh of
    Right r  -> pure r
    Left e -> throwM err500 {errBody = "Bad swarm state conversion: " <> toS e}

-- | Handler for checking the alive state of the service
aliveH :: KatipContext m => m Bool
aliveH = pure True

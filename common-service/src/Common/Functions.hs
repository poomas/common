module Common.Functions
  ( shutdownMaster
  , setDynamicPermitData
  , lflToDpd
  , dpdToLfl

  , setScribeSeverity
  , getAllSeverities
  , handleStdMessages

  )
where

import Imports

import RIO.Text        as T (uncons)
import Servant.Client  (showBaseUrl)

import Common.Type.App (CommonApp (..), HasCommonApp (..), getDynPermMap,
                        getDynPermMapRef, getKillSwitch)
import Poomas.API      (LogFilterLoad (..), MessageData (..), MessageLoad (..))
import Scribe.Katip    (DynamicPermitData (..), defDynamicPermitData, logDebugKS,
                        logWarnKS)


type FuncsMonad m = (KatipContext m, HasCommonApp m)


------------------------------------------------------------------------------------------
-- Shutdown
------------------------------------------------------------------------------------------

-- | Shutdown the master
shutdownMaster :: FuncsMonad m => m ()
shutdownMaster = do
  flip putMVar () =<< getKillSwitch
  threadDelay $ 1_000__000 -- 1 sec


------------------------------------------------------------------------------------------
-- Debug
------------------------------------------------------------------------------------------

-- | Change Severity for specified Scribe
setScribeSeverity :: FuncsMonad m => Text -> Severity -> m Bool
setScribeSeverity scribeName severity = do
  ioDL  <- getDynPermMapRef
  dlMap <- readIORef ioDL
  case lookup scribeName dlMap of
    Nothing -> pure False
    Just dl -> do
      let newDL = dl{dpdSeverity = Right [severity]}
      writeIORef ioDL $ insertMap scribeName newDL dlMap
      pure True


-- | Return Severities of all Scribes
getAllSeverities :: FuncsMonad m => m [(Text,Severity)]
getAllSeverities =
  -- map (second dpdSeverity) . mapToList <$> getDynPermMap
  pure []


handleStdMessages :: FuncsMonad m => MessageLoad -> m (Maybe MessageData)
handleStdMessages MessageLoad{..} = case msgLData of
  MessageLog lfl -> do
    b <- setDynamicPermitData "poomas" lfl
    logDebugKS "Common" ("Change DPD: " <> tshow b) >> pure Nothing
  MessageShutdown () -> do
    logWarnKS "Common" $ toS (showBaseUrl msgLFrom) <> " asked to shutdown the server!"
    shutdownMaster >> pure Nothing
  MessageCustom t -> do
    logDebugKS "Common" $ "Custom message from " <> toS (showBaseUrl msgLFrom) <> ":\n"
        <> t
    pure $ Just (MessageCustom t)


-- | Change Debug settings for specified Scribe
setDynamicPermitData :: (HasCommonApp m, MonadIO m) => Text -> LogFilterLoad -> m Bool
setDynamicPermitData "all" lfl =
  and <$> (mapM (`setDynamicPermitData` lfl)
              .  filter (/= "all")
              .  keys
             =<< getDynPermMap
          )
setDynamicPermitData scribeName lfl = do
  sev    <- bool (Left WarningS) (Right []) . isDevelopment <$> getCommonApp
  dpdRef <- getDynPermMapRef
  dpdMap <- readIORef dpdRef
  case lookup scribeName dpdMap of
    Nothing  -> pure False
    Just dpd -> do
      writeIORef dpdRef $ insertMap scribeName (lflToDpd sev lfl dpd) dpdMap
      pure True

------------------------------------------------------------------------------------------
-- Converters
------------------------------------------------------------------------------------------

-- | Update `DynamicPermitData` with some changed values
lflToDpd :: Either Severity [Severity] -> LogFilterLoad -> DynamicPermitData
         -> DynamicPermitData
lflToDpd sev LogFilterLoad{..} curr@DynamicPermitData{..} = do
  let dpd = bool curr (defDynamicPermitData sev) lflReset
  dpd { dpdSeverity    = maybe dpdSeverity         severityToDpd lflSeverity
      , dpdMsgPrefixes = maybe dpdMsgPrefixes (map textsToDpd)   lflPrefixes
      , dpdNamespaces  = maybe dpdNamespaces  (map textsToDpd)   lflNamespaces
      , dpdPeriod      =       dpdPeriod       <|>               lflPeriod
      , dpdHost        =       dpdHost         <|>               lflHost
      , dpdModule      =       dpdModule       <|>               lflModule
      }

  where
    severityToDpd [s] = Left s
    severityToDpd xs  = Right xs

    textsToDpd m = case T.uncons m of
                     Just ('-',x) -> Left  x
                     Just (_,  x) -> Right x
                     Nothing      -> Right m

-- | Convert logger data to OTW format
dpdToLfl :: DynamicPermitData -> LogFilterLoad
dpdToLfl DynamicPermitData{..} = do
  LogFilterLoad { lflReset      = False
                , lflSeverity   = Just $ either (:[]) id dpdSeverity
                , lflPrefixes   = Just $ map dpdToTexts dpdMsgPrefixes
                , lflNamespaces = Just $ map dpdToTexts dpdNamespaces
                , lflPeriod     = dpdPeriod
                , lflHost       = dpdHost
                , lflModule     = dpdModule
                }

  where
    dpdToTexts (Left a) = "-" <> a
    dpdToTexts (Right a) = a

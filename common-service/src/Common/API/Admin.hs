module Common.API.Admin where

import Imports             hiding (lookup)

import Servant.API.Generic ((:-))

import Poomas.API          (KnownHubs)


------------------------------------------------------------------------------------------
-- Shutdown server
------------------------------------------------------------------------------------------
type StopRoute =
     Summary "Shutdowns the server"
  :> Get '[JSON] Text


------------------------------------------------------------------------------------------
-- Debug settings
------------------------------------------------------------------------------------------
type DebugSetRoute =
     Summary "Changes dynamically Severity for the app"
  :> Capture' '[Description "Name of the scribe to modify or 'all' for all scribes"]
               "scribe"
                Text
  :> Capture' '[Description "New Severity to be set"]
               "level"
               Severity
    -- TODO: add how long before converting back to original levels
    -- TODO: add prefixes
    -- TODO: add namespaces
  :> Post '[JSON] Text

type DebugInfoRoute = Get '[JSON] [(Text, Severity)]


------------------------------------------------------------------------------------------
-- Swarm info
------------------------------------------------------------------------------------------
type SwarmStateRoute =
     Summary "Return the current state of the swarm."
  :> Get '[JSON] KnownHubs

------------------------------------------------------------------------------------------
-- Alive check
------------------------------------------------------------------------------------------
type AliveRoute =
     Summary "Return true is server is alive."
  :> Get '[JSON] Bool

------------------------------------------------------------------------------------------
-- API
------------------------------------------------------------------------------------------
data CommonAdminRoutes r = CommonAdminRoutes
  { carDebugSetR   :: r :- "debug"            :> DebugSetRoute
  , carDebugInfoR  :: r :- "debug"            :> DebugInfoRoute
  , carShutdownR   :: r :- "shutdown"         :> StopRoute
  , carAliveR      :: r :- "alive"            :> AliveRoute
  , carSwarmStateR :: r :- "swarm" :> "state" :> SwarmStateRoute
  } deriving (Generic)

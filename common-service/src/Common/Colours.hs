module Common.Colours where

import RIO

-- echo -e "Default \e[32;44mGreen"

colorize :: Text -> Text -> Text
colorize c s = "\ESC["<> c <> "m" <> s <> "\ESC[0m"


colorList :: [(String, Int)]
colorList =
  [ ("boldBright",      1)
  , ("dim",             2)
  , ("underlined",      4)
  , ("blink",           5)

  -- Foreground
  , ("fBlack",         30)
  , ("fRed",           31)
  , ("fGreen",         32)
  , ("fYellow",        33)
  , ("fBlue",          34)
  , ("fMagenta",       35)
  , ("fCyan",          36)
  , ("fGrayLight",     37)
  , ("fGrayDark ",     90)
  , ("fRedLight ",     91)
  , ("fGreenLight ",   92)
  , ("fYellowLight ",  93)
  , ("fBlueLight ",    94)
  , ("fMagentaLight ", 95)
  , ("fCyanLight ",    96)
  , ("fWhite ",        97)

  -- Background
  , ("bBlack",         40)
  , ("bRed",           41)
  , ("bGreen",         42)
  , ("bYellow",        43)
  , ("bBlue",          44)
  , ("bMagenta",       45)
  , ("bCyan",          46)
  , ("bGrayLight",     47)
  , ("bGrayDark",     100)
  , ("bRedLight",     101)
  , ("bGreenLight",   102)
  , ("bYellowLight",  103)
  , ("bBlueLight",    104)
  , ("bMagentaLight", 105)
  , ("bCyanLight",    106)
  , ("bWhite",        107)
    ]

boldBright,dim,underlined,blink,fBlack,fRed,fGreen,fYellow,fBlue,fMagenta,fCyan,fGrayLight
  ,fGrayDark,fRedLight,fGreenLight,fYellowLight,fBlueLight,fMagentaLight,fCyanLight,fWhite
  ,bBlack,bRed,bGreen,bYellow,bBlue,bMagenta,bCyan,bGrayLight,bGrayDark,bRedLight
  ,bGreenLight,bYellowLight,bBlueLight,bMagentaLight,bCyanLight,bWhite :: Text -> Text


boldBright = colorize "1"
dim        = colorize "2"
underlined = colorize "4"
blink      = colorize "5"

-- Foreground
fBlack        = colorize "30"
fRed          = colorize "31"
fGreen        = colorize "32"
fYellow       = colorize "33"
fBlue         = colorize "34"
fMagenta      = colorize "35"
fCyan         = colorize "36"
fGrayLight    = colorize "37"
fGrayDark     = colorize "90"
fRedLight     = colorize "91"
fGreenLight   = colorize "92"
fYellowLight  = colorize "93"
fBlueLight    = colorize "94"
fMagentaLight = colorize "95"
fCyanLight    = colorize "96"
fWhite        = colorize "97"

-- Background
bBlack        = colorize " 40"
bRed          = colorize "41"
bGreen        = colorize "42"
bYellow       = colorize "43"
bBlue         = colorize "44"
bMagenta      = colorize "45"
bCyan         = colorize "46"
bGrayLight    = colorize "47"
bGrayDark     = colorize "100"
bRedLight     = colorize "101"
bGreenLight   = colorize "102"
bYellowLight  = colorize "103"
bBlueLight    = colorize "104"
bMagentaLight = colorize "105"
bCyanLight    = colorize "106"
bWhite        = colorize "107"

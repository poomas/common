module Common.API.Swagger where

import           Imports              hiding (serveDirectoryWebApp)

import           Data.Aeson.Encode.Pretty   (encodePretty)
import qualified RIO.ByteString.Lazy       as BL
import qualified Data.HashMap.Strict.InsOrd as InsOrd
import           Data.Swagger               (Swagger (..), URL (..), description, info,
                                             license, title, url, version)
import qualified Data.Swagger               as Swg
import qualified Data.Text                  as T
import           Servant.Swagger            (HasSwagger (..))


-- | Output generated @swagger.json@ file.
commonGenSwagger :: HasSwagger api => Text -> Text -> Proxy api -> FilePath -> IO ()
commonGenSwagger t d api fNm = BL.writeFile fNm . encodePretty $ mkSwagger t d api

-- | Create `Swagger` docs for `AllAPIs` API
mkSwagger :: HasSwagger api => Text -> Text -> Proxy api -> Swagger
mkSwagger ttl desc api = setOperationIds mkOperationId $ toSwagger
  api
  & info.title       .~ ttl
  & info.version     .~ "0.1.0"
  & info.description ?~ desc
  & info.license     ?~ ("BSD3" & url
                            ?~ URL "https://opensource.org/licenses/BSD-3-Clause"
                        )
  where
    -- Makes concatenated CamelCase string, i.e. "getDelaySeconds"
    mkOperationId verb = T.concat
                       . (:) verb
                       . map T.toTitle
                       . filter (\t -> not (T.null t || "{" `T.isPrefixOf` t))
                       . T.split (\c -> not (isAlphaNum c || c `elem` allowedChars))
    allowedChars = ['-', '{',  '}']


-- | Set 'operationId' to each path with supplied 'convert' function
setOperationIds :: (Text -> Text -> Text) -> Swagger -> Swagger
setOperationIds convert = over Swg.paths (InsOrd.mapWithKey
  (\(T.pack -> path) ->
     let f verb nm  = over verb (setId nm <$>)
         setId verb = over Swg.operationId (<|> Just (convert verb path))
      in f Swg.get     "get"
       . f Swg.put     "put"
       . f Swg.post    "post"
       . f Swg.delete  "delete"
       . f Swg.options "options"
       . f Swg.head_   "head"
       . f Swg.patch   "patch"))

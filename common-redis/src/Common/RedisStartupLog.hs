module Common.RedisStartupLog where

import ClassyPrelude     as X hiding (Handler, getCurrentTime)

import Data.String.Conv  (toS)
import Data.Text         as T

import Common.Type.Redis (HasRedisConfig (..), RedisConfiguration (..))

------------------------------------------------------------------------------------------
-- Initial display of 'DbConfig' settings
------------------------------------------------------------------------------------------

redisStartupLog :: HasRedisConfig a => a -> Text
redisStartupLog (getRedisConfig -> RedisConfiguration{..}) = T.concat
  [ "Redis\n"
  , "\n - Host:      " <> toS redHost
  , "\n - Port:      " <> tshow redPort
  , "\n - Auth:      " <> if isNothing redAuth then ("Not set"::Text) else "Set"
  , "\n - Database:  " <> tshow redDatabase
  , "\n - Max. cons: " <> tshow redMaxConns
  , "\n - Idle time: " <> tshow redMaxIdleTime
  , "\n - Timeout:   " <> tshow redTimeout
  , "\n - TTL:       " <> tshow redTTL
  ]

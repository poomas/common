module Common.Utils
  ( tryError
  , throwIfEither

  , fst3, snd3, thd3
  , updLocal
  , mkStatusError

  , prompt

  , ekgMetric
  , ekgTime
  , time'n'Counter

  , out
  , showBUrl
  , showBUrls

  )
where

import Imports

import Common.Type.App (HasCommonApp, getEkgMetrics)
import Servant.Client  (BaseUrl (..))
import System.Clock    (Clock (..), diffTimeSpec, getTime, toNanoSecs)

import Poomas.API      (HubUri, getHubUri)

------------------------------------------------------------------------------------------
-- Utils
------------------------------------------------------------------------------------------
fst3 :: (a,b,c) -> a
fst3 (x,_,_) = x
{-# INLINE fst3 #-}

snd3 :: (a,b,c) -> b
snd3 (_,x,_) = x
{-# INLINE snd3 #-}

thd3 :: (a,b,c) -> c
thd3 (_,_,x) = x
{-# INLINE thd3 #-}


-- | Show msg on console and wait for any `String`, which is ingored.
prompt :: Text -> IO ()
prompt msg = do
  say msg
  _ <- getLine
  return ()

-- | Convert `MonadError` exception to `Either` type
tryError :: (MonadUnliftIO m, Exception e) => m b -> m (Either e b)
tryError m = (Right <$> m) `catch` (return . Left)

-- | Throw `Exception` if value is `Left`, otherwise return the value in `Right`.
throwIfEither :: (Exception e, MonadThrow m) => Either e a -> m a
throwIfEither = either throwM pure


-- | Update Lens part by supplied function
updLocal :: MonadReader s m => Lens' s a -> (a -> a) -> m v -> m v
updLocal l f = local (\r -> r & l %~ f)


-- | Incorporate text within exception
mkStatusError :: ServerError -> ByteString -> ServerError
mkStatusError e t =
  e { errHeaders = [ (hContentType, "text/plain; charset=utf-8") ]
    , errBody    = toS t
    }

------------------------------------------------------------------------------------------
-- Metrics
------------------------------------------------------------------------------------------

-- | Run EKG function on 'Master's store, if given
-- ekgMetric :: (MonadIO m, MonadReader App m) => ReaderT Metrics m () -> m ()
ekgMetric :: (Monad m, HasCommonApp m) => ReaderT Metrics m b -> m b
ekgMetric meter = runReaderT meter =<< getEkgMetrics

-- | Measure time spent in given action under specified name
ekgTime :: (MonadUnliftIO m, HasCommonApp m) => Text -> m b -> m b
ekgTime name a = measureTime =<< getEkgMetrics
  where
    measureTime m     = bracket (liftIO $ getTime Monotonic) (storeTime m) (const a)
    diffMS end start  = fromInteger @Double (toNanoSecs (diffTimeSpec end start)) / 1_000_000
    storeTime m start = do
      end <- liftIO $ getTime Monotonic
      runReaderT (distribution ("Poomas." <> name) $ diffMS end start) m

-- | Measure time spent in given action under specified name and
--   increase the same name counter by 1
time'n'Counter :: (HasCommonApp m, MonadUnliftIO m) => Text -> m b -> m b
time'n'Counter name a = do
  ekgMetric (increment $ "Poomas." <> name <> "-count")
  ekgTime (name <> "-time") a


------------------------------------------------------------------------------------------
-- Display helpers
------------------------------------------------------------------------------------------

-- | Display BaseUrl. Holder must have instance of 'HubUri'
showBUrl :: HubUri a => a -> Text
showBUrl (getHubUri -> BaseUrl{..}) = toS baseUrlHost <> ":" <> tshow baseUrlPort

-- | Display a container of BaseUrls. Each item must have instance of 'HubUri'
showBUrls :: (HubUri a, MonoFoldable (t Text), Functor t, Element (t Text) ~ Text)
          => t a -> Text
showBUrls xs = intercalate "; " $ map showBUrl xs


------------------------------------------------------------------------------------------
-- For debugging
------------------------------------------------------------------------------------------

out :: MonadIO m => Int -> Text -> m ()
out nls m = do
  t <- toS . formatTime defaultTimeLocale "%M:%S" <$> liftIO getCurrentTime
  say $ replicate nls '\n' <> t <> ": " <> m

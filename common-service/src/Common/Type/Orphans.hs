{-# OPTIONS_GHC -fno-warn-orphans #-}
module Common.Type.Orphans ( module X ) where

import RIO
import RIO.Orphans           as X
import Scribe.Orphans.Scribe as X

import Data.Swagger          (NamedSchema (..), Swagger (..), SwaggerType (..),
                              ToSchema (..), description, type_)
import Lens.Micro.Platform   ((?~))



------------------------------------------------------------------------------------------
-- Orphan instances for Swagger
------------------------------------------------------------------------------------------
instance ToSchema Swagger where
  declareNamedSchema _ = pure . NamedSchema (Just "Swagger") $ mempty
    & type_       .~ Just SwaggerObject
    & description ?~ "JSON with Swagger definition"

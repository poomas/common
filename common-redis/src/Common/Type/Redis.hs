module Common.Type.Redis
  ( RedisConfiguration(..)
  , HasRedisConfig (..)
  , HasRedisConn (..)
  , mkRedConnInfo
  )
where

import ClassyPrelude    hiding (toLower)

import Data.Aeson       (FromJSON (..), Options (..), ToJSON (..), defaultOptions,
                         genericToJSON, withObject, (.!=), (.:), (.:?))
import Data.Char        as C
import Data.String.Conv (toS)
import Data.Time.Clock  (NominalDiffTime)
import Database.Redis   as R


-- | Extract 'RedisConfiguration'
class HasRedisConfig a where getRedisConfig :: a -> RedisConfiguration

-- | Extract Redis 'Connection' from environment
class HasRedisConn   m where  getRedisConn :: m Connection

-- | Info for connecting to Redis database
data RedisConfiguration = RedisConfiguration
  { redHost        :: String
  , redPort        :: Int
  , redAuth        :: Maybe String
  , redDatabase    :: Integer
  , redMaxConns    :: Int
  , redMaxIdleTime :: NominalDiffTime
  , redTimeout     :: Maybe NominalDiffTime
  , redTTL         :: Integer
  } deriving (Show, Eq, Generic, Typeable)
instance ToJSON   RedisConfiguration where toJSON = genericToJSON $ jsonOpts 3
instance FromJSON RedisConfiguration where
  parseJSON = withObject "RedisConfiguration" $ \o -> do
    redHost        <- o .:  "hostname"
    redPort        <- o .:? "port"     .!= 6379
    redAuth        <- o .:? "auth"
    redDatabase    <- o .:  "dbNumber"
    redMaxConns    <- o .:? "maxConns" .!= 50
    redMaxIdleTime <- o .:? "maxIdle"  .!= 30
    redTimeout     <- o .:? "timeout"
    redTTL         <- o .:? "ttl"      .!= 3600
    return RedisConfiguration{..}

-- | Prepare `Redis` `ConnectInfo` from configuration
mkRedConnInfo :: RedisConfiguration -> ConnectInfo
mkRedConnInfo RedisConfiguration{..}  = R.defaultConnectInfo
  { connectHost           = redHost
  , connectPort           = PortNumber $ fromIntegral redPort -- Redis default port
  , connectAuth           = toS <$> redAuth                   -- No password
  , connectDatabase       = redDatabase                       -- SELECT database 0
  , connectMaxConnections = redMaxConns                       -- Up to 50 connections
  , connectMaxIdleTime    = redMaxIdleTime                    -- Keep open for 30 seconds
  , connectTimeout        = redTimeout                        -- Don't add timeout logic
  }

-- | Options for automatic FromJSON and ToJSON generation.
jsonOpts :: Int -> Options
jsonOpts n = defaultOptions { fieldLabelModifier = fixField }
 where
  fixField t = case drop n t of
    (x : xs) -> C.toLower x : xs
    _        -> []

module Common.Main where

import Imports

import Options.Applicative (Parser, ParserInfo, execParser, fullDesc, header, help,
                            helper, info, long, progDesc, short, showDefault, strOption,
                            value)
import RIO.Directory       (doesFileExist)


import Common.Run          (AppPrep (..), cleanUp, getAPAppName, startApp)
import Common.Type.App     (CommonApp (..), HasCommonAppL (..), HasCommonConfigL (..))
import Common.Type.Config  (CommonConfig (..), EkgConfig (..))
import Poomas.Server       (PhcEnv, forkPoomasService, stopHubRefresher, addInfo)


-- | Any cmd line arguments overrides config
newtype CommonArgs = CommonArgs
  { argConfigFile :: FilePath -- ^ config file to use
  } deriving (Show)


getCommonArgs :: IO CommonArgs
getCommonArgs = do
  CommonArgs{..} <- execParser commonPInfo
  doesFileExist argConfigFile >>= \case
    True  -> pure CommonArgs{..}
    False -> error $ configFileError argConfigFile


commonPInfo :: ParserInfo CommonArgs
commonPInfo = info (commonArgs <**> helper)
             (fullDesc <> progDesc "Poomas server"
                       <> header   "Poomas")

commonArgs :: Parser CommonArgs
commonArgs = CommonArgs <$>
  strOption (long "cfgFile"  <> short 'c'
          <> value "server/config/config.yaml" <> showDefault
          <> help "Path of configuration file")

configFileError :: String -> String
configFileError f = "\nError!!!"
  <> "  - Specified configuration file `" <> f <> "` does not exist!"
  <> "  - Specify existing and valid configuration file, or start without "
  <> "`-c` param so the default configuration file will be used"


withKillSwitch :: (HasCommonConfigL appCfg, HasCommonAppL app)
                => (FilePath -> IO appCfg)
                -> (appCfg -> PhcEnv -> IO (AppPrep app))
                -> ((appCfg, AppPrep app, PhcEnv) -> IO appResult)
                -> IO appResult
withKillSwitch prepConfig' prepApp' a = do
  CommonArgs{..} <- getCommonArgs

  appCfg <- prepConfig' argConfigFile               -- Prepare app config
  (phcEnv, swThread)  <- forkPoomasService (cCfgPoomas $ appCfg ^. commonConfigL) -- Start poomas service

  _ <- case cCfgEkg $ appCfg ^. commonConfigL of
         Nothing               -> pure ()
         Just EkgConfig { .. } -> addInfo phcEnv ("EKG" , toS $ ekgBind <> ":" <> show ekgPort)

  appPrep@AppPrep{..} <- prepApp' appCfg phcEnv          -- get master data
  appThread           <- async . void $ startApp appPrep -- start master app

  ret <- a (appCfg,appPrep, phcEnv)

  -- Wait until MVar gets filled, meaning a shutDown request has been made
  -- if app thread dies, this will not wait forever but will exit
  void . takeMVar . killSwitch $ apApp ^. commonAppL

  cleanUp $ apApp ^. commonAppL -- master app cleanup
  stopHubRefresher phcEnv       -- stop poomas refresher service
  void $ cancel swThread        -- stop poomas service
  void $ cancel appThread       -- stop main server
  say $ "- Server '" <> getAPAppName appPrep <> "` stopped"
  pure ret

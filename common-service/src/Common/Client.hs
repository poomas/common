{-# LANGUAGE AllowAmbiguousTypes #-}
module Common.Client
  ( TypedClientEnv (..)
  , TypedService (..)
  , HasTypedService (..)

  , getCEnvByClass
  , getCEnvByService
  , getServiceByName

  , withPoomasClient
  , runPoomasClientEither
  , runPoomasClient
  , rethrow

  )
  where

import           Imports                  hiding (lookup, responseBody)
import qualified RIO.Set                  as S
import           Servant.Client           (ClientEnv (..), ClientError (..), mkClientEnv)
import           Servant.Client.Streaming (ClientM, responseBody, responseStatusCode,
                                           runClientM, withClientM)


import           Common.Type.App          (HasCommonApp, getHttpMan, getPhcEnv,
                                           getPoomasConfig)
import           Poomas.API               (Service (..), hubNeed, needService, pCfgLocal,
                                           serviceName)
import           Poomas.Server            (PoomasConfig, getBaseUrl)


data TypedClientEnv a = TypedClientEnv {unTypedCE  :: ClientEnv}
data TypedService   a = TypedService   {unTypedSrv :: Service}

class HasTypedService a where
  getTypedServiceName :: Text

  findTypedService :: PoomasConfig -> Either Text (TypedService a)
  findTypedService cfg = do
    let srvName =getTypedServiceName @a
    maybe (Left srvName) (Right . TypedService)
      . headMay
      . toList
      . S.filter ((==) srvName . (^. serviceName))
      $ cfg ^. pCfgLocal . hubNeed . needService


type PoomasMonad m = (MonadIO m, MonadThrow m, HasCommonApp m)

withPoomasClient :: forall srv m a b. (HasTypedService srv, PoomasMonad m)
                  => ClientM a -> (Either ClientError a -> IO b) -> m b
withPoomasClient f a = do
  ce <- unTypedCE <$> getCEnvByClass @srv
  liftIO $ withClientM f ce a

runPoomasClientEither :: forall srv m a. (HasTypedService srv, PoomasMonad m, NFData a)
                      => ClientM a -> m (Either ClientError a)
runPoomasClientEither f = do
  ce <- unTypedCE <$> getCEnvByClass @srv
  liftIO $ runClientM f ce

runPoomasClient :: forall srv m a. (HasTypedService srv, PoomasMonad m, NFData a)
               => ClientM a -> m a
runPoomasClient = rethrow <=< runPoomasClientEither @srv


getCEnvByClass :: forall a m. ( MonadIO m, MonadThrow m
                              , HasCommonApp m, HasTypedService a)
               => m (TypedClientEnv a)
getCEnvByClass = getCEnvByService =<< getServiceByName @a


-- | Find Create typed 'ClientEnv' for specified service.
--   - Service must be defined in the 'Need' part of 'PoomasConfig'
--   - takes first if more services with same name (different versions) defined
--   - if service not found, a 404 exception is returned
getServiceByName :: (HasTypedService a, MonadIO m, MonadThrow m, HasCommonApp m)
                 => m (TypedService a)
getServiceByName = do
  findTypedService <$> getPoomasConfig >>= \case
    Right srv -> pure srv
    Left  srv -> errorExit srv "not defined in 'Need' part of the config!"


-- | Create typed 'ClientEnv' for specified service based on dynamic info from swarm..
--   - Service must be defined in the 'Need' part of 'PoomasConfig'
--   - takes first if more services with same name (different versions) defined
--   - if service not found, a 404 exception is returned
getCEnvByService :: forall a m. (MonadIO m, MonadThrow m, HasCommonApp m)
                 => TypedService a -> m (TypedClientEnv a)
getCEnvByService (TypedService srv) = do
  phcEnv  <- getPhcEnv
  man <- getHttpMan
  liftIO (getBaseUrl phcEnv srv) >>= \case
    Just url -> pure . TypedClientEnv $ mkClientEnv man url
    Nothing  -> errorExit (srv ^. serviceName) "not available in Poomas!"


-- | Common error exit for both above functions
errorExit :: (MonadIO m, MonadThrow m) => Text -> Text -> m a
errorExit srv msg = say ("'" <> srv <> "' service " <> msg) >> throwM err404



-- | Throw error if result is `Left ClientError'
rethrow :: (MonadThrow m) => Either ClientError a -> m a
rethrow (Left (FailureResponse _ r)) = case statusCode $ responseStatusCode r of
  400 -> err err400 $ responseBody r
  403 -> err err403 $ responseBody r
  404 -> err err404 ("" :: ByteString)
  405 -> err err405 ("" :: ByteString)
  _   -> throwM . userError
             $ show (statusCode $ responseStatusCode r) <> ": "
            <> toS  (responseBody r)

  where
    err e bs = throwM . mkStatusError e $ toS bs
rethrow (Left  e) = throwM e
rethrow (Right a) = pure a


-- | Incorporate text within exception
mkStatusError :: ServerError -> ByteString -> ServerError
mkStatusError e t =
  e { errHeaders = [ (hContentType, "text/plain; charset=utf-8") ]
    , errBody    = toS t
    }

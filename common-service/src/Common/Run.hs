module Common.Run where

import Imports

import Control.Concurrent          (killThread)
import Network.Wai                 (Middleware)
import Network.Wai.Handler.Warp    (defaultSettings, runSettings, setBeforeMainLoop,
                                    setHost, setPort, setServerName)
import Network.Wai.Handler.WarpTLS (runTLS, tlsSettingsMemory)

import Common.Type.App             (CommonApp (..), HasCommonApp (..), HasCommonAppL (..),
                                    HasCommonConfigL (..), commonConfigL)
import Common.Type.Config          (CommonConfig (..), InternalFileData (..),
                                    parseCommonConfig)
import Poomas.Server               (PhcEnv, sendLog)
import Scribe.Class                (AnyScribe (..), KatipScribe (..))
import Scribe.Katip                (DynPermMap, DynamicPermitData (..), klsLogEnv,
                                    logInfoK, mkScribes)
import Scribe.Poomas               (defPoomasScribe)

import qualified RIO.Map                           as M

-- | All data needed for application to run. It contains active connections, pools, etc.
--   so should be properly dismissed
data AppPrep app = AppPrep
  { apKatipLE    :: LogEnv
  , apApp        :: app
  , apWaiApp     :: Application
  , apMiddleware :: Middleware
  }

-- | Extract application name form AppPrep
getAPAppName :: (StringConv Text a, HasCommonAppL app) => AppPrep app -> a
getAPAppName = toS . cCfgAppName . getAPCommonConfig

-- | Extract CommonConfig from AppPrep
getAPCommonConfig :: HasCommonAppL app => AppPrep app -> CommonConfig
getAPCommonConfig = capConfig . view commonAppL . apApp

type MkApp appCfg app = (appCfg
                      -> LogEnv
                      -> IORef DynPermMap
                      -> PhcEnv
                      -> KatipContextT IO (app, Application, Middleware))

prepCommonApp :: HasCommonConfigL appCfg
              => appCfg -> PhcEnv -> MkApp appCfg app -> IO (AppPrep app)
prepCommonApp appConfig phcEnv mkApp = do
  let cCfg = appConfig ^. commonConfigL
  (dpm, apKatipLE) <- prepScribes cCfg phcEnv
  now              <- getCurrentTime
  (apApp, apWaiApp, apMiddleware) <- runKatipContextT apKatipLE () "init" $ do
    logInfoK . fromString $ "Common config prepared on: " <> show now
    mkApp appConfig apKatipLE dpm phcEnv
  return AppPrep{..}


-- | Start the application server. It is expected to run the server as a forked
--   process, and `shutdownSwitch` enables "almost" clean shutting it down. It is
--   an empty MVar, and when it gets filled, the main thread, which is waiting on
--   this MVar, will exit, closing forked server as well.
--   Server is run in `try` block, so any exception should result in `Left` exit.
prepCommonConfig :: FromJSON AnyScribe => FilePath -> IO CommonConfig
prepCommonConfig fNm = do
  isDev <- ("production" `notElem`) <$> getArgs
  parseCommonConfig isDev [fNm] []

prepScribes :: CommonConfig -> PhcEnv -> IO (IORef DynPermMap, LogEnv)
prepScribes CommonConfig{..} phcEnv = do
  let dpm = M.fromList
          . zip (map scribeName loggers)
          $ repeat DynamicPermitData
            { dpdSeverity    = bool (Left WarningS) (Right []) cCfgIsDev
            , dpdMsgPrefixes = []
            , dpdNamespaces  = []
            , dpdPeriod      = Nothing
            , dpdHost        = Nothing
            , dpdModule      = Nothing
            }
  dpmRef <- newIORef dpm
  (dpmRef,) <$> mkScribes dpmRef
                         (Namespace [cCfgAppName])
                         (Environment $ bool "production" "development" cCfgIsDev)
                          loggers


  where
    loggers = if cCfgLogToPoomas
      then AnyScribe (defPoomasScribe "poomas" . Just $ sendLog phcEnv) : cCfgLoggers
      else cCfgLoggers

-- | Start the application with prepared data
startApp :: HasCommonAppL app => AppPrep app -> IO (Either SomeException ())
startApp AppPrep{..} = do
  r <- runKatipContextT apKatipLE () "init"
         $ runWaiApp (apApp ^. commonAppL) (apMiddleware apWaiApp)
  print r
  return r

-- | Actions that should be performed on shutting down the server
cleanUp :: CommonApp -> IO ()
cleanUp CommonApp{..} = do
  void . closeScribes =<< view klsLogEnv <$> readIORef capKatip
  void $ killThread `traverse` capEkgServer
  void $ tryPutMVar killSwitch ()


-- | Run WAI app in `try` block and return `Either` result.
runWaiApp :: (KatipContext m, Exception e) => CommonApp -> Application -> m (Either e ())
runWaiApp CommonApp{..} app = do
  let settings = setPort (fromIntegral capPort)
               . setHost (fromString $ toS capHost) -- bind only to specific IP
               . setServerName (toS capAppName)
               . setBeforeMainLoop preRunActions
               $ defaultSettings
  liftIO . try $ runSettings settings app

  where
    preRunActions = do
      say $ "Listening on " <> toS capHost <> ":" <> tshow capPort


-- | Run WAI app in `try` block and return `Either` result.
runWaiAppTls :: (KatipContext m, Exception e, HasCommonApp m)
             => InternalFileData -> Application -> m (Either e ())
runWaiAppTls InternalFileData{..} app = do
  cap@CommonApp{..} <- getCommonApp
  let settings = setPort (fromIntegral capPort)
               . setHost (fromString $ toS capHost) -- bind only to specific IP
               . setServerName (toS capAppName)
               . setBeforeMainLoop (preRunActions cap)
               $ defaultSettings
      tlsSett = tlsSettingsMemory (toS ifdTLSCert) (toS ifdTLSKey)
  liftIO . try $ if isDevelopment
                   then runSettings    settings app
                   else runTLS tlsSett settings app

  where
    preRunActions  CommonApp{..} = do
      say $ "Listening on " <> toS capHost <> ":" <> tshow capPort

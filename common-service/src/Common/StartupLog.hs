{-# LANGUAGE QuasiQuotes #-}
module Common.StartupLog where

import           Imports

import qualified RIO.Text              as T
import           Servant.Client        (BaseUrl, showBaseUrl)
import           Text.Shakespeare.Text (st)


import           Common.Type.Config    (BasicAuthConfig (..), CommonConfig (..),
                                        EkgConfig (..))
import           Poomas.API            (Monitor (..), OfferedService (..), Service (..),
                                        Version (..), hubNeed, hubOffer, hubUri,
                                        needGateway, needInfo, needMonitor, needService,
                                        offerCustom, offerGateway, offerInfo,
                                        offerMonitor, offerService, offerSrvService,
                                        offerSrvUri, pCfgBeacons, pCfgLocal,
                                        serviceLBType, serviceName, serviceVersion,
                                        showLoadBalancer)

------------------------------------------------------------------------------------------
-- Initial display of app settings and configuration
------------------------------------------------------------------------------------------

-- | Create log line with current configuration from YAML and/or ENV, maybe changed by
--   automatic host and port selection
startupLog :: CommonConfig -> Text
startupLog cCfg@CommonConfig{..} = T.concat $
  "Running in " <> bool "PRODUCTION" "DEVELOPMENT" cCfgIsDev <> " mode\n"
  :  startupLogHeader cCfg
  :  section "Beacons"       (map (startupLogBeacons 3) (cCfgPoomas ^. pCfgBeacons))
  <> section "Master needs"  (logNeeds  (cCfgPoomas ^. pCfgLocal . hubNeed))
  <> section "Master offers" (logOffers (cCfgPoomas ^. pCfgLocal . hubOffer))

  where
    section    t xs = if null xs then mempty else ("\n - "   <> t <> ": ") : xs
    subSection t xs = if null xs then mempty else ("\n   - " <> t <> ": ") : xs
    logOffers o =
        subSection "Services" (map (startupLogOffService  5) $ toList (o ^. offerService))
     <> subSection "Info"     (map (startupLogOffInfoItem 5) $ toList (o ^. offerInfo))
     <> subSection "Monitor"  (map (startupLogMonitor     5) $ toList (o ^. offerMonitor))
     <> subSection "Gateway"  (map (startupLogAny         5) $ toList (o ^. offerGateway))
     <> subSection "Custom"   (map (startupLogAny         5) $ toList (o ^. offerCustom))

    logNeeds o =
        subSection "Services" (map (startupLogService      5) $ toList (o ^. needService))
     <> subSection "Info"     (map (startupLogNeedInfoItem 5) $ toList (o ^. needInfo))
     <> subSection "Monitor"  (map (startupLogMonitor      5) $ toList (o ^. needMonitor))
     <> subSection "Gateway"  (map (startupLogAny          5) $ toList (o ^. needGateway))


startupLogHeader :: CommonConfig -> Text
startupLogHeader CommonConfig{..} = do
  let master = cCfgPoomas ^. pCfgLocal
      basicAuth = case cCfgFullSiteBasicAuth of
        Nothing                  -> "*** Not set ***"
        Just BasicAuthConfig{..} -> concat
          [ "\n - Realm: " <> bAuthRealm
          , "\n - Creds: " <> toS bAuthCreds
          , "\n - User:  " <> bAuthUser
          , "\n - Pwd:   " <> bAuthPwd
          ]
  [st|
Initial Application settings
 - Host:        #{cCfgAppHost}
 - Port:        #{tshow cCfgAppPort}
 - ID name:     #{cCfgAppName}
 - Storage Dir: #{cCfgStorageDir}
 - BasicAuth:   #{basicAuth}
Metrics
 - Port: #{maybe "none" show $ ekgPort <$> cCfgEkg}
 - URL:  #{fromMaybe "none"  $ ekgBind <$> cCfgEkg}
Poomas configuration
 - Hub url: #{showBaseUrl $ master ^. hubUri}|]

startupLogBeacons :: Int -> BaseUrl -> Text
startupLogBeacons n bu = [st|#{ident n}#{showBaseUrl bu}|]

startupLogService :: Int -> Service -> Text
startupLogService n s = [st|#{ident n}#{s ^. serviceName}:#{unVersion $ s ^. serviceVersion}|]

startupLogOffService :: Int -> OfferedService -> Text
startupLogOffService n o = do
  let srv = o ^. offerSrvService
      uri = o ^. offerSrvUri
      service = startupLogService n srv
      sch = showLoadBalancer $ srv ^. serviceLBType :: Text
  [st|#{service}@#{showBaseUrl uri}<?>#{sch}|]

startupLogNeedInfoItem :: Int -> Text -> Text
startupLogNeedInfoItem n k = [st|#{ident n}#{k}|]

startupLogOffInfoItem :: Int -> (Text,Text) -> Text
startupLogOffInfoItem n (k,v) = [st|#{ident n}#{k}: #{v}|]

startupLogMonitor :: Int -> Monitor -> Text
startupLogMonitor n m = [st|#{ident n}#{showMonitor m}|]

startupLogAny :: Show a => Int -> a -> Text
startupLogAny n g = [st|#{ident n}#{show g}|]

ident :: Int -> Text
ident n = "\n" <> replicate n ' '  <> "- "

showMonitor :: Monitor -> Text
showMonitor Monitor{..} = monitorName <> "@" <> tshow monitorEvent

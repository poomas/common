module Common.Type.App where

import Imports                           hiding (responseStatus)

import Network.Wai                       (Middleware, responseStatus)
import Network.Wai.Metrics               (metrics, registerWaiMetrics)
import Network.Wai.Middleware.AddHeaders (addHeaders)
import Network.Wai.Middleware.Gzip       (def, gzip)
import Network.Wai.Middleware.HttpAuth   (authRealm, basicAuth)
import Network.Wai.Middleware.Timeout    (timeoutStatus)
import Servant.Client                    (BaseUrl)
import Servant.Ekg                       (HasEndpoint, monitorEndpoints)
import System.Metrics                    (newStore)
import System.Remote.Monitoring          (forkServer, serverMetricStore, serverThreadId)


import Common.Type.Config                (BasicAuthConfig (..), CommonConfig (..),
                                          EkgConfig (..))
import Poomas.Server                     (PhcEnv, PoomasConfig, sendException)
import Scribe.Katip                      as X (DynPermMap, KatipLS (..), katipReqLogger,
                                               logDebugKS)


-- | Application state, both static and dynamic parts
data CommonApp = CommonApp
  { --  Static
    capHost       :: Text           -- ^ http/s address
  , capPort       :: Word           -- ^ port on which the app is run
  , capBaseUrl    :: Maybe BaseUrl  -- ^ BaseUrl of this app
  , capAppName    :: Text           -- ^ identification of this app
  , capStorageDir :: FilePath       -- ^ the main file storage
  , isDevelopment :: Bool           -- ^ is app in development mode
  , capMetrics    :: Metrics        -- ^ host and port for metrics server (ekg)
  , capHttpMan    :: Manager        -- ^ http manager
  , capConfig     :: CommonConfig   -- ^ app config read from file/environment
  , capEkgServer  :: Maybe ThreadId -- ^ thread for killing EKG server
  , capPhcEnv     :: PhcEnv         -- ^ comm channel to Poomas hub

  --  Dynamic
  , killSwitch    :: MVar ()          -- ^ putting () in MVar causes server to stop
  , capKatip      :: IORef KatipLS    -- ^ Katip config, for dynamic log filtering
  , capDynPermMap :: IORef DynPermMap -- ^ data for dynamic logging
  }


-- | Clases for accesing fields in CommonApp
class HasCommonApp     m where getCommonApp  :: m       CommonApp
class HasCommonAppL    c where commonAppL    :: Lens' c CommonApp
class HasCommonConfigL c where commonConfigL :: Lens' c CommonConfig

instance HasCommonConfigL CommonApp where
  commonConfigL = lens capConfig (\x y -> x { capConfig = y })

commonConfig :: HasCommonAppL app => Lens' app CommonConfig
commonConfig = lens (^. commonAppL . commonConfigL)
                    (\x y -> x & commonAppL . commonConfigL .~ y)

katipLSLRef :: (HasCommonAppL app) => Lens' app (IORef KatipLS)
katipLSLRef = lens (capKatip . view commonAppL)
                   (\x y -> x & commonAppL .~ (x ^. commonAppL){capKatip = y})


-- katipLS :: (HasCommonAppL app) => Lens' app (IORef KatipLS)
-- katipLS = lens (^. commonAppL . commonConfigL . katipLSL)
--                (\x y -> x & commonAppL . katipLSL .~ y)
-- katipLSL = lens (^. commonAppL . commonConfigL)
--                 (\x y -> x & commonAppL . commonConfigL .~ y)
-- instance HasCommonConfigL App  where
--   commonConfigL = lens (^. commonAppL . commonConfigL)
--                        (\x y -> x & commonAppL . commonConfigL .~ y)

-- instance HasKatipLSL   App where
--   katipLSL = lens (view katipLSL . appCommon) (\x y -> x & commonAppL . katipLSL .~ y)


------------------------------------------------------------------------------------------
-- CommonApp getters
------------------------------------------------------------------------------------------

getAppHost :: (Functor m, HasCommonApp m) => m Text
getAppHost = capHost <$> getCommonApp

getAppPort :: (Functor m, HasCommonApp m) => m Word
getAppPort = capPort <$> getCommonApp

getAppName :: (Functor m, HasCommonApp m) => m Text
getAppName = capAppName <$> getCommonApp

getStorageDir :: (Functor m, HasCommonApp m) => m FilePath
getStorageDir = capStorageDir <$> getCommonApp

getEkgMetrics :: (Functor m, HasCommonApp m) => m Metrics
getEkgMetrics = capMetrics <$> getCommonApp

getHttpMan :: (Functor m, HasCommonApp m) => m Manager
getHttpMan = capHttpMan <$> getCommonApp

getPhcEnv :: (Functor m, HasCommonApp m) => m PhcEnv
getPhcEnv = capPhcEnv <$> getCommonApp

getCommonConfig :: (Functor m, HasCommonApp m) => m CommonConfig
getCommonConfig = capConfig <$> getCommonApp

getPoomasConfig :: (Functor m, HasCommonApp m) => m PoomasConfig
getPoomasConfig = cCfgPoomas <$> getCommonConfig

getKillSwitch :: (Functor m, HasCommonApp m) => m (MVar ())
getKillSwitch = killSwitch <$> getCommonApp


------------------------------------------------------------------------------------------
-- Katip related
------------------------------------------------------------------------------------------
getDynPermMapRef :: (Functor m, HasCommonApp m) => m (IORef DynPermMap)
getDynPermMapRef = capDynPermMap <$> getCommonApp

getDynPermMap :: (HasCommonApp m, MonadIO m) => m DynPermMap
getDynPermMap = readIORef =<< getDynPermMapRef

getKatipLSRef :: (Functor m, HasCommonApp m) => m (IORef KatipLS)
getKatipLSRef = capKatip <$> getCommonApp

getKatipLS :: (HasCommonApp m, MonadIO m) => m KatipLS
getKatipLS = readIORef =<< getKatipLSRef


-- | Update Lens part of appKatip IORef by supplied function
localKatip :: (HasCommonApp m, MonadReader app m, MonadIO m, HasCommonAppL app)
           => Lens' KatipLS a -> (a -> a) -> m v -> m v
localKatip l f a = do
  newKatip <- liftIO . (newIORef . (l %~ f)) =<< getKatipLS
  local (\app -> app & katipLSLRef .~ newKatip) a

-- | Get Lens part of appKatip IORef
viewKatip :: (HasCommonApp m, MonadIO m) => Getting b KatipLS b -> m b
viewKatip l = view l <$> getKatipLS



-- | Prepare `CommonApp` state from external configuration (file and ENV).
--   If configured, forks EKG server, prepares metrics, DB pool and logging.
--   Prepares a list of possible WAI middlewares:
--     - metrics
--     - Basic HTTP Auth for the whole site with 1 credential
--     - add predefined static `Headers` to each response
--     - WAI request logger
--     - specified `Request` timeout
--     - gzip payload
--   Return created `App` and list of `Middleware's.
mkCommonApp :: (KatipContext m, HasEndpoint api)
            => CommonConfig
            -> LogEnv
            -> IORef DynPermMap
            -> PhcEnv
            -> Proxy api
            -> m (CommonApp, [Middleware])
mkCommonApp conf@CommonConfig{..} katipLE dpm phcEnv fullApi = do
  (ekgServer, store) <- liftIO $ case cCfgEkg of
    Nothing            -> (Nothing, ) <$> newStore
    Just EkgConfig{..} -> (Just &&& serverMetricStore)
                       <$> forkServer (toS ekgBind) (fromIntegral ekgPort)

  logDebugKS "Startup" "Making Foundation"
  killSwitch      <- liftIO newEmptyMVar
  metricWai       <- liftIO $ registerWaiMetrics store
  modMetr         <- liftIO $ initializeWith     store
  httpManager     <- liftIO $ newManager defaultManagerSettings
  ekgMiddleware   <- liftIO $ mkEkgMiddleware fullApi
  katipIORef      <- liftIO $ newIORef KatipLS { _klsLogEnv    = katipLE
                                               , _klsContext   = liftPayload ()
                                               , _klsNamespace = Namespace []
                                               }
  let app = CommonApp
        { capHost       = cCfgAppHost
        , capPort       = cCfgAppPort
        , capBaseUrl    = Nothing
        , capAppName    = cCfgAppName
        , capStorageDir = cCfgStorageDir
        , killSwitch    = killSwitch
        , isDevelopment = cCfgIsDev
        , capMetrics    = modMetr
        , capHttpMan    = httpManager
        , capConfig     = conf
        , capEkgServer  = serverThreadId <$> ekgServer
        , capPhcEnv     = phcEnv
        , capKatip      = katipIORef
        , capDynPermMap = dpm
        }

  return ( app
         , [ ekgMiddleware
           , metrics metricWai
           , maybe id (timeoutStatus status408) cCfgRequestTimeout
           , bool (addHeaders $ map (bimap toS toS) cCfgAlwaysAddHeaders) id
             $ null cCfgAlwaysAddHeaders
           , basicAuthWare cCfgFullSiteBasicAuth
           , gzip def
           , exceptionLogger
           ]
           <> bool [] [katipReqLogger katipIORef] cCfgLogRequest
         )
  where
    mkEkgMiddleware api = monitorEndpoints api =<< newStore
    basicAuthWare  Nothing                   = id
    basicAuthWare (Just BasicAuthConfig{..}) = do
      let verifyAuth u p = pure $ u == toS bAuthUser && p == toS bAuthPwd
          mkAuthSettings = (fromString bAuthRealm) { authRealm = toS bAuthCreds }
      basicAuth verifyAuth mkAuthSettings

    exceptionLogger :: Middleware
    exceptionLogger app req sendResponse = app req $ \res -> do
      let Status code msg = responseStatus res
      when (code >= 500 && code < 600) .
        void $ sendException phcEnv (tshow code) (toS msg)
      sendResponse res

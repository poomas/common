module Common.DbStartupLog where

import ClassyPrelude             as X hiding (Handler, getCurrentTime)

import Common.PGUtils (HasDbConfig(..), DbConfig(..))

------------------------------------------------------------------------------------------
-- Initial display of 'DbConfig' settings
------------------------------------------------------------------------------------------

dbStartupLog :: HasDbConfig a => a -> Text
dbStartupLog (getDbConfig -> DbConfig{..}) = concat
  [ "\nDatabase"
  , "\n - Host:    " <> dbHost
  , "\n - Port:    " <> tshow dbPort
  , "\n - DB name: " <> dbName
  , "\n - User:    " <> dbUser
  , "\n - Pool:    " <> tshow dbPoolSize
  ]
